package fr.univ_lyon1.info.m1.mes.model;


public class Dentist extends HealthProfessional {
    public Dentist(final String name, final MES mes) {
        super(name, mes);
    }
}
